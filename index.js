const express = require("express")
const server = express()
const router = express.Router()
const fs = require('fs')

server.use(express.json({extended: true}))

const readFile = () => {
    const content = fs.readFileSync('./data/items.json', 'utf-8')
    return JSON.parse(content)
}

const writeFile = (content) => {
    const updateFile = JSON.stringify(content)
    fs.writeFileSync('./data/items.json', updateFile, 'utf-8')
}

//listar 
router.get('/', (req, res) => {
    const content = readFile()
    res.send(content)
})
//listar por id
router.get('/:id', (req, res) => {
    const {id} = req.params
	const currentContent = readFile()
	const selectedItem = currentContent.find((item)=> item.id ===id)
	res.send(selectedItem)
})

//cadastar
router.post('/', (req, res) => {
    const { nome, email, telefone, endereco, observacoes } = req.body
    const currentContent = readFile()
    const id = Math.random().toString(32).substr(2, 9)
    currentContent.push({ id, nome, email, telefone, endereco, observacoes })
    writeFile(currentContent)
    res.send({ id, nome, email, telefone, endereco, observacoes })
})

//editar
router.put('/:id', (req, res) => {
    const {id} = req.params

    const { nome, email, telefone, endereco, observacoes } = req.body

    const currentContent = readFile()
    const selectedItem = currentContent.findIndex((item) => item.id === id)

    const { id: cId, nome: cNome, email: cEmail, telefone: cTelefone, endereco: cEndereco, observacoes: cObservacoes } = currentContent[selectedItem]

    const newObject = {
        id: cId,
        nome: nome ? nome: cNome,
        email: email ? email: cEmail,
        telefone: telefone ? telefone: cTelefone,
		endereco: endereco ? endereco: cEndereco,
		observacoes: observacoes ? observacoes: cObservacoes 
    }

    currentContent[selectedItem] = newObject
    writeFile(currentContent)

    res.send(newObject)
})
// deletar
router.delete('/:id', (req, res) => {
    const { id } = req.params
    const currentContent = readFile()
    const selectedItem = currentContent.findIndex((item) => item.id === id)
    currentContent.splice(selectedItem, 1)
    writeFile(currentContent)
    res.send(true)
})

server.use(router)

server.listen(3000, () => {
    console.log('Rodando servidor na porta 3000')
})